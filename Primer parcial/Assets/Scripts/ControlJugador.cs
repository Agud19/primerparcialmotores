using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ControlJugador : MonoBehaviour{

    private Rigidbody rb;
    private BoxCollider col;

    public AudioClip ClipSonido;
    public AudioSource Source;

    public Camera Camara;

    public float rapidezDesplazamiento = 10.0f;
    private float gravedad = 3f;

    private int cont;

    [SerializeField] Transform PisoCheck;
    [SerializeField] LayerMask Piso;

    void Start(){
        cont = 0;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update(){

        rb.AddForce(Vector3.down * gravedad);

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        if (Input.GetKeyDown("w")){
            Source.PlayOneShot(ClipSonido, 1);
        }

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape")){
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
