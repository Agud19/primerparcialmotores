using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour{

    public GameObject jugador;

    float tiempoRestante;

    void Start(){
        ComenzarJuego();
    }

    void Update(){
        if (Input.GetKeyDown(KeyCode.R)){
            SceneManager.LoadScene("SampleScene");
        }
        if (tiempoRestante == 0){
            SceneManager.LoadScene("SampleScene");
        }
    }

    void ComenzarJuego(){
        jugador.transform.position = new Vector3(-91.1f, 1.5f, -111.5f);
        StartCoroutine(ComenzarCronometro(900));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro = 900){
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0){
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
}