using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScriptColeccionables : MonoBehaviour{

    public AudioClip ClipSonido;
    public AudioSource Source;

    private void OnTriggerEnter(Collider other){
        if (other.gameObject.CompareTag("Player") == true){
            Destroy(gameObject);
            Source.PlayOneShot(ClipSonido, 1);
        }
    }
}
